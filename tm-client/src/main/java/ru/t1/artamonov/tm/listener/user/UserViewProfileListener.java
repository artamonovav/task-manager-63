package ru.t1.artamonov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.model.UserDTO;
import ru.t1.artamonov.tm.dto.request.UserProfileRequest;
import ru.t1.artamonov.tm.enumerated.Role;
import ru.t1.artamonov.tm.event.ConsoleEvent;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    private static final String NAME = "view-user-profile";

    @NotNull
    private static final String DESCRIPTION = "view profile of current user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userViewProfileListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @NotNull final UserDTO user = authEndpoint.profile(request).getUser();
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
