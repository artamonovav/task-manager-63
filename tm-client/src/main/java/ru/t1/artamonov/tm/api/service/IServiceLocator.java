package ru.t1.artamonov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.api.endpoint.ISystemEndpoint;

public interface IServiceLocator {

    @NotNull
    ITokenService getTokenService();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

}
