package ru.t1.artamonov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.event.ConsoleEvent;
import ru.t1.artamonov.tm.listener.AbstractListener;

@Component
public final class ApplicationHelpListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "help";

    @NotNull
    private static final String ARGUMENT = "-h";

    @NotNull
    private static final String DESCRIPTION = "Display list of terminal command.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@applicationHelpListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[HELP]");
        for (@Nullable final AbstractListener abstractListener : listeners) {
            System.out.println(abstractListener);
        }
    }

}
