package ru.t1.artamonov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.artamonov.tm.dto.request.DataJsonLoadFasterXmlRequest;
import ru.t1.artamonov.tm.event.ConsoleEvent;

@Component
public final class DataJsonLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-json";

    @NotNull
    private static final String DESCRIPTION = "Load data from json file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonLoadFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA LOAD JSON]");
        @NotNull DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(getToken());
        domainEndpointClient.loadDataJsonFasterXml(request);
    }

}
