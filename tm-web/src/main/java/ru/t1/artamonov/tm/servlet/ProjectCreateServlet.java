package ru.t1.artamonov.tm.servlet;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.repository.ProjectRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/create/*")
public class ProjectCreateServlet extends HttpServlet {

    @Override
    protected void doGet(@NotNull final HttpServletRequest req, @NotNull final HttpServletResponse resp) throws IOException {
        ProjectRepository.getInstance().create();
        resp.sendRedirect("/projects");
    }

}
