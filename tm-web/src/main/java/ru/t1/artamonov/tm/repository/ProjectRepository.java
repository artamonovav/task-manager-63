package ru.t1.artamonov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();
    @Nullable
    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("SECOND"));
        add(new Project("FIRST"));
        add(new Project("THOURTH"));
        add(new Project("THIRD"));
    }

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        add(new Project("New Project " + System.currentTimeMillis()));
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    @Nullable
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}
