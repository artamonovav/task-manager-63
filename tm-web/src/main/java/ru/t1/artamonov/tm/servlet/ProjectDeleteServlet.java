package ru.t1.artamonov.tm.servlet;

import org.jetbrains.annotations.NotNull;
import ru.t1.artamonov.tm.repository.ProjectRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/delete/*")
public class ProjectDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(@NotNull final HttpServletRequest req, @NotNull final HttpServletResponse resp) throws IOException {
        @NotNull final String id = req.getParameter("id");
        ProjectRepository.getInstance().removeById(id);
        resp.sendRedirect("/projects");
    }

}
