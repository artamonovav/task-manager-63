package ru.t1.artamonov.tm.jms.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
