//package ru.t1.artamonov.tm.service;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.Assert;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//import ru.t1.artamonov.tm.api.service.IConnectionService;
//import ru.t1.artamonov.tm.api.service.dto.IProjectDTOService;
//import ru.t1.artamonov.tm.api.service.dto.ITaskDTOService;
//import ru.t1.artamonov.tm.api.service.dto.IUserDTOService;
//import ru.t1.artamonov.tm.dto.model.TaskDTO;
//import ru.t1.artamonov.tm.enumerated.Status;
//import ru.t1.artamonov.tm.marker.UnitCategory;
//import ru.t1.artamonov.tm.service.dto.ProjectDTOService;
//import ru.t1.artamonov.tm.service.dto.TaskDTOService;
//import ru.t1.artamonov.tm.service.dto.UserDTOService;
//
//import static ru.t1.artamonov.tm.constant.ProjectTestData.USER1_PROJECT1;
//import static ru.t1.artamonov.tm.constant.ProjectTestData.USER2_PROJECT1;
//import static ru.t1.artamonov.tm.constant.TaskTestData.*;
//import static ru.t1.artamonov.tm.constant.UserTestData.*;
//
//@Category(UnitCategory.class)
//public final class TaskServiceTest {
//
//    @Nullable
//    private static ITaskDTOService taskService;
//
//    @Nullable
//    private static IUserDTOService userService;
//
//    @BeforeClass
//    public static void init() {
//        @NotNull PropertyService propertyService = new PropertyService();
//        @NotNull IConnectionService connectionService = new ConnectionService(propertyService);
//        @NotNull IProjectDTOService projectService = new ProjectDTOService(connectionService);
//        taskService = new TaskDTOService(connectionService);
//        userService = new UserDTOService(connectionService, propertyService);
//        if (userService.findOneById(USER1.getId()) == null) userService.add(USER1);
//        if (userService.findOneById(USER2.getId()) == null) userService.add(USER2);
//        if (userService.findOneById(ADMIN.getId()) == null) userService.add(ADMIN);
//        projectService.add(USER1_PROJECT1);
//        projectService.add(USER2_PROJECT1);
//    }
//
//    @Test
//    public void add() {
//        taskService.add(USER1_TASK1);
//        Assert.assertTrue(taskService.existsById(USER1_TASK1.getId()));
//        taskService.remove(USER1_TASK1);
//        Assert.assertFalse(taskService.existsById(USER1_TASK1.getId()));
//    }
//
//    @Test
//    public void addByUserId() {
//        taskService.add(USER1.getId(), USER1_TASK1);
//        Assert.assertTrue(taskService.existsById(USER1_TASK1.getId()));
//        taskService.remove(USER1_TASK1);
//        Assert.assertFalse(taskService.existsById(USER1_TASK1.getId()));
//    }
//
//    @Test
//    public void clearByUserId() {
//        taskService.clear(USER2.getId());
//        taskService.clear(USER1.getId());
//        taskService.add(USER1_TASK_LIST);
//        Assert.assertEquals(3, taskService.getSize(USER1.getId()));
//        taskService.clear(USER2.getId());
//        Assert.assertEquals(0, taskService.getSize(USER2.getId()));
//        taskService.clear(USER1.getId());
//        Assert.assertEquals(0, taskService.getSize(USER1.getId()));
//    }
//
//    @Test
//    public void findAllByUserId() {
//        taskService.add(TASK_LIST);
//        Assert.assertEquals(USER1_TASK_LIST.size(), taskService.findAll(USER1.getId()).size());
//        taskService.removeAll(TASK_LIST);
//    }
//
//    @Test
//    public void findOneByIdByUserId() {
//        taskService.add(USER1_TASK1);
//        Assert.assertEquals(USER1_TASK1.getId(), taskService.findOneById(USER1.getId(), USER1_TASK1.getId()).getId());
//        taskService.remove(USER1_TASK1);
//        Assert.assertFalse(taskService.existsById(USER1_TASK1.getId()));
//    }
//
//    @Test
//    public void removeByUserId() {
//        taskService.add(USER1_TASK1);
//        Assert.assertEquals(USER1_TASK1.getId(), taskService.remove(USER1.getId(), USER1_TASK1).getId());
//        taskService.remove(USER1_TASK1);
//        Assert.assertFalse(taskService.existsById(USER1_TASK1.getId()));
//    }
//
//    @Test
//    public void removeByIdByUserId() {
//        taskService.add(USER1_TASK1);
//        taskService.add(USER2_TASK1);
//        Assert.assertEquals(USER1_TASK1.getId(), taskService.removeById(USER1.getId(), USER1_TASK1.getId()).getId());
//        Assert.assertFalse(taskService.existsById(USER1_TASK1.getId()));
//        Assert.assertTrue(taskService.existsById(USER2_TASK1.getId()));
//        taskService.remove(USER1_TASK1);
//        taskService.remove(USER2_TASK1);
//    }
//
//    @Test
//    public void existsByIdByUserId() {
//        taskService.add(USER1_TASK1);
//        Assert.assertTrue(taskService.existsById(USER1_TASK1.getId()));
//        Assert.assertFalse(taskService.existsById(USER2_TASK1.getId()));
//        taskService.remove(USER1_TASK1);
//    }
//
//    @Test
//    public void changeTaskStatusById() {
//        taskService.add(USER1_TASK1);
//        @NotNull TaskDTO task = taskService.findOneById(USER1_TASK1.getId());
//        Assert.assertNotNull(task);
//        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
//        taskService.changeTaskStatusById(task.getUserId(), task.getId(), Status.IN_PROGRESS);
//        @NotNull TaskDTO task1 = taskService.findOneById(USER1_TASK1.getId());
//        Assert.assertEquals(Status.IN_PROGRESS, task1.getStatus());
//        taskService.remove(USER1_TASK1);
//    }
//
//    @Test
//    public void createTaskName() {
//        @NotNull TaskDTO task = taskService.create(USER1.getId(), "test_task");
//        @NotNull TaskDTO task1 = taskService.findOneById(task.getId());
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task1);
//        Assert.assertEquals(task.getId(), task1.getId());
//        Assert.assertEquals("test_task", task1.getName());
//        Assert.assertEquals(USER1.getId(), task1.getUserId());
//        taskService.remove(task);
//    }
//
//    @Test
//    public void createTaskNameDescription() {
//        @NotNull TaskDTO task = taskService.create(USER1.getId(), "test_task", "test_description");
//        @NotNull TaskDTO task1 = taskService.findOneById(task.getId());
//        Assert.assertNotNull(task);
//        Assert.assertNotNull(task1);
//        Assert.assertEquals(task.getId(), task1.getId());
//        Assert.assertEquals("test_task", task1.getName());
//        Assert.assertEquals("test_description", task1.getDescription());
//        Assert.assertEquals(USER1.getId(), task1.getUserId());
//        taskService.remove(task);
//    }
//
//    @Test
//    public void updateById() {
//        @NotNull TaskDTO task = taskService.create(USER1.getId(), "test_task", "test_description");
//        taskService.updateById(USER1.getId(), task.getId(), "new name", "new description");
//        @NotNull TaskDTO task1 = taskService.findOneById(task.getId());
//        Assert.assertEquals("new name", task1.getName());
//        Assert.assertEquals("new description", task1.getDescription());
//    }
//
//}
